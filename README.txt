
Installation
------------
1) Place this module directory in your "modules" folder (this will usually be
   "sites/all/modules/"). Don't install your module in Drupal core's "modules"
   folder, since that will cause problems and is bad practice in general. If
   "sites/all/modules" doesn't exist yet, just create it.

2) Apply the patch to xmlsitemap module (xmlsitemap.patch). Instructions can be
   found at http://drupal.org/patch/apply.
   However, a quick reminder:
     a) Change the directory to the root directory of your Drupal core:
          cd /htdocs/example.com
     b) Change the directory to the directory of your Drupal contributes module:
          cd sites/all/modules
     b) Copy the patch to this directory
          cp xmlsitemap_images/xmlsitemap.patch xmlsitemap
     c) Change the directory to the directory of your Drupal contributes module:
          cd xmlsitemap
     d) Apply the patch:
          patch -p0 < xmlsitemap.patch

3) Enable the module.

4) Run rebuild of the XML sitemap links at admin/config/search/xmlsitemap/rebuild
   because all node links need to be regenerated.

5) Regenerate sitemap cache files at admin/config/search/xmlsitemap.